// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    // apiKey: '<your-key>',
    // authDomain: '<your-project-authdomain>',
    // databaseURL: '<your-database-URL>',
    // projectId: '<your-project-id>',
    // storageBucket: '<your-storage-bucket>',
    // messagingSenderId: '<your-messaging-sender-id>',
    // appId: '<your-app-id>',
    // measurementId: '<your-measurement-id>'

    apiKey: "AIzaSyBZdlZ76WyeF2wJNtr2E24pYfiHrmkCZ7k",
    authDomain: "fir-fotos-b6a84.firebaseapp.com",
    projectId: "fir-fotos-b6a84",
    storageBucket: "fir-fotos-b6a84.appspot.com",
    messagingSenderId: "187414820762",
    appId: "1:187414820762:web:75d31f93b98b8a962fac23"

    
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
