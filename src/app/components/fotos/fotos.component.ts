import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Item {nombre: string; url: string};

@Component({
  selector: 'app-fotos',
  templateUrl: './fotos.component.html',
  styleUrls: ['./fotos.component.css']
})
export class FotosComponent implements OnInit {

  items: Observable<Item[]>; // any[]

  constructor(firestore: AngularFirestore) {

    this.items = firestore.collection<Item>('img').valueChanges();
   }

  ngOnInit(): void {
    
  }

}
