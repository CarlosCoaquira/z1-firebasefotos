import { Component, OnInit } from '@angular/core';
import { FileItem } from '../../models/file-items';
import { CargaFotoService } from '../../services/carga-foto.service';

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styleUrls: ['./carga.component.css']
})
export class CargaComponent implements OnInit {

  isDrop: boolean = false;
  archivos: FileItem[] = [];

  constructor(public _cargaFoto: CargaFotoService) { }

  ngOnInit(): void {
  }

  // testUpElement( event){
  //   console.log(event);
  // }

  cargarImagen(){
    this._cargaFoto.cargarImageFirebase(this.archivos);
  }

  limpiarArchivos(){
    this.archivos = [];
  }

}
