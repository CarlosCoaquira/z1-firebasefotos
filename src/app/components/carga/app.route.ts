import { Routes, RouterModule } from "@angular/router";
import { FotosComponent } from '../fotos/fotos.component';
import { CargaComponent } from './carga.component';


const RUTAS: Routes = [
    { path: 'fotos', component: FotosComponent },
    { path: 'carga', component: CargaComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'fotos' }
];

export const APP_ROUTES = RouterModule.forRoot(RUTAS);