import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage'; //nuevo
import * as firebase from 'firebase';
import { FileItem } from '../models/file-items';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CargaFotoService {

  private CARPETA_IMAGENES = 'img';

  constructor( private firestore: AngularFirestore,
              private storage: AngularFireStorage ) {
      
   }

   cargarImageFirebase(imagenes: FileItem[]){

    for (const item of imagenes) {
 
      item.estaSubiendo = true;
      if ( item.progreso >= 100 ) {
        continue;
      }
 
      const file = item.archivo;
      const nombreEnFirebase = `${ item.nombreArchivo }-${ Date.now() }`
      const filePath = `${ this.CARPETA_IMAGENES }/${ nombreEnFirebase }`;
      const fileRef = this.storage.ref( filePath );
      const uploadTask = this.storage.upload(filePath, file);
 
      // con esta función nos suscribimos a los cambios en el progreso
      uploadTask.percentageChanges().subscribe( resp => item.progreso = resp);
      // obtengo el url de descarga cuando este disponible
      uploadTask.snapshotChanges().pipe(
        finalize(
          () => fileRef.getDownloadURL().subscribe( url => {
            console.log('Imagen cargada con exito');
            item.url = url;
            item.estaSubiendo = false;
            this.guardarImagen({
              nombre: nombreEnFirebase, // item.nombreArchivo,
              url: item.url
            });
          })
        )
      ).subscribe();
    }

   }

   private guardarImagen( imagen: {nombre: string, url: string} ) {
     this.firestore.collection(`/${this.CARPETA_IMAGENES}`)
     .add(imagen);
   }
}
