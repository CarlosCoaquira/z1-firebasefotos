import { Directive, EventEmitter, ElementRef, HostListener, Input, Output } from '@angular/core';
import { FileItem } from '../models/file-items';
import {map} from 'rxjs/operators';

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {

  @Input () archivos: FileItem[] = [];
  @Output () mouseSobre: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  @HostListener('dragover', ['$event'])
  public mouseEntro ( event: any) {
    this.mouseSobre.emit( true);
    this._prevenirDetener(event);
  }

  @HostListener('dragleave', ['$event'])
  public mouseSalio ( event: any) {
    this.mouseSobre.emit( false);
  }

  @HostListener('drop', ['$event'])
  public mouseSin ( event: any) {
    

    const transfer = this._getTransfer(event);

    if (!transfer) {
      return;
    }

    this._extraeArchivos (transfer.files);

    this._prevenirDetener(event);
    this.mouseSobre.emit( false);
  }

  // para compatibilidad de los navegadores
  private _getTransfer( event:any) {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private _extraeArchivos(archivosLista: FileList) {
    
    for( const propiedad in Object.getOwnPropertyNames(archivosLista)) {
      const arcchivoTemporal = archivosLista[propiedad];

      if (this._archivoOk(arcchivoTemporal) ) {
        const nuevoArchivo = new FileItem(arcchivoTemporal);
        this.archivos.push(nuevoArchivo);
      }
    }

    // console.log(this.archivos);
  }

  //validaciones

  // 0.-
  private _archivoOk(archivo: File): boolean {
    if ( !this._archivoExiste(archivo.name) && this._esImagen(archivo.type) ) {
      return true;
    } else {
      return false;
    }
  }
  // 1.- prevenir que el explorador habra la imagen arrastrada en una pagina nueva
  private _prevenirDetener ( event ) {

    event.preventDefault();
    event.stopPropagation();
  }

  // Existe previamente
  private _archivoExiste( nombreArchivo: string): boolean {
    for (const archivo of this.archivos) {
      if (archivo.nombreArchivo === nombreArchivo) {
        console.log(`Archivo ${nombreArchivo} ya existe`);
        return true;
      }
    }
    return false;
  }

  // es una imagen?
  private _esImagen(tipoArchivo: string): boolean {
    return (tipoArchivo==='' || tipoArchivo === undefined ) ? false : tipoArchivo.startsWith('image');

  }


}
