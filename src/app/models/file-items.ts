export class FileItem{
    public archivo: File;
    public nombreArchivo: string;
    public url: string;
    public estaSubiendo: boolean;
    public progreso: number;

    constructor ( archivo_ : File ) {
        this.archivo = archivo_;
        this.nombreArchivo = archivo_.name;

        this.estaSubiendo = false;
        this.progreso = 0;
    }
}